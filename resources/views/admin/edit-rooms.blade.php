@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{ route('room.update', ['id' => $room->id]) }}">
        Name
        <input type="text" name="name" value="{{ $room->name }}"><br>
        Price
        <input type="number" name="price" value="{{ $room->price }}"><br>
        Square
        <input type="number" name="square" value="{{ $room->square }}"><br>
        Floor
        <input type="number" name="floor" value="{{ $room->floor }}"><br>
        Windows
        <input type="number" name="windows" value="{{ $room->windows }}"><br>
        Tables
        <input type="number" name="tables" value="{{ $room->tables }}"><br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" value="Submit">
    </form>
@endsection
