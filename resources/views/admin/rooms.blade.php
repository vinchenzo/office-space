@extends('layouts.app')

@section('content')
    <div class="col-md-10">
    <table border="1" width="100%" cellpadding="5">
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Square, m<sup>2</sup></th>
            <th>Floor</th>
            <th>Windows</th>
            <th>Tables</th>
            <th>Action</th>
        </tr>
        @foreach($rooms as $room)
            <tr>
                <td>{{ $room->name }}</td>
                <td>{{ $room->price }}</td>
                <td>{{ $room->square }}</td>
                <td>{{ $room->floor }}</td>
                <td>{{ $room->windows }}</td>
                <td>{{ $room->tables }}</td>
                <td class="text-center">    <a class="edit" href="{{ route('room.get', ['id' => $room->id]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {{--<a href="{{ route('room.delete', ['id' => $room->id]) }}"> (delete)</a></td>--}}
            </tr>
        @endforeach
    </table>
    {{ $rooms->links() }}
@endsection
