@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{ route('customer.update', ['id' => $user->id]) }}">
        First Name
        <input type="text" name="first_name" value="{{ $user->first_name }}"><br>
        Last Name
        <input type="text" name="last_name" value="{{ $user->last_name }}"><br>
        Email
        <input type="email" name="email" value="{{ $user->email }}"><br>
        Company
        <input type="text" name="company" value="{{ $user->company }}"><br>
        Phone
        <input type="tel" name="phone" value="{{ $user->phone }}"><br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" value="Submit">
    </form>
@endsection
