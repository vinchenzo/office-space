@extends('layouts.app')

@section('content')
    <div class="col-md-10">
    <table border="1" width="100%" cellpadding="5">
        <tr>
            <th>User name</th>
            <th>User Email</th>
            <th>Room Name</th>
            <th>From Date</th>
            <th>To Date</th>
        </tr>
        @foreach($reserves as $reserve)
            <tr>
                <td>{{ $reserve->user->first_name . ' ' . $reserve->user->last_name}}</td>
                <td>{{ $reserve->user->email }}</td>
                <td>{{ $reserve->room->name }}</td>
                <td>{{ $reserve->from_date }}</td>
                <td>{{ $reserve->to_date }}</td>
                <td>
                    @if (!$reserve->confirmed)
                    <a href="{{ route('reserves.approve', ['id' => $reserve->id]) }}">Approve</a><br>
                    <a href="{{ route('reserves.delete', ['id' => $reserve->id]) }}">Delete</a>
                    @endif
                    @if ($reserve->confirmed)
                            <a href="{{ route('reserves.delete', ['id' => $reserve->id]) }}">Delete</a>
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
    {{ $reserves->links() }}
@endsection
