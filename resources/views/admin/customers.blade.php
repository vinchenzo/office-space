@extends('layouts.app')

@section('content')
    <div class="col-md-10">
    <table border="1" width="100%" cellpadding="5">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Company</th>
            <th>Phone</th>
            <th>Created</th>
        </tr>
        @foreach($users as $user)
            <tr>
                <td>
                    {{ $user->first_name . ' ' . $user->last_name }}
                    <a href="{{ route('customer.get', ['id' => $user->id]) }}"> (edit)</a>
                    <a href="{{ route('customer.delete', ['id' => $user->id]) }}"> (delete)</a>
                </td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->company }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->created_at }}</td>
            </tr>
        @endforeach
    </table>
    {{ $users->links() }}
    </div>
@endsection
