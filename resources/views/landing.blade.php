<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">


    <!-- Styles -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/jquery.fullpage.min.css">
    <link rel="stylesheet" href="css/main.css">
    <!-- Scripts -->

    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgr5na6iqLjLnOw5TfwMARS5JowsCWaU4&callback=initMap">
    </script>
    <script src="js/jquery.visible.min.js"></script>
    <script src="js/jquery.fullpage.extensions.min.js"></script>
    <script src="js/jquery.fullpage.min.js"></script>
    <script src="js/scripts.js"></script>


</head>
<body>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div id="menu">
    <div class="buttons-menu">
        <div class="open-menu"></div>
    </div>
    <ul>
        <li data-menuanchor="firstPage"><a  href="#firstpage" class="loggo-main">
                <img class="imag" src="img/logo_green.png" alt="">
                <img class="imag hidden" src="img/logo_sec.png" alt="">
            </a></li>
        <li data-menuanchor="secpage"><a  href="#secpage">Rooms</a></li>
        <li data-menuanchor="thirdpage"><a  href="#thirdpage">Amenities</a></li>
        <li data-menuanchor="fourthpage"><a  href="#fourthpage">Neighborhood</a></li>
        <li data-menuanchor="fivethpage"><a  href="#fivethpage">Testimonials</a></li>
    </ul>
    <div class="bottom-info">
        <div class="bottom-info-item">
            <span>Tanbruckgasse 3, </span>
            <span>Top 3, 1120 Wien</span>
        </div>
        <div class="bottom-info-item">
            <span>+436991123456789 </span>
            <span>servus@tanbruck-offices.wien</span>
        </div>
        <div class="links-soc">
            <a href="">Facebook</a>
            <a href="">Imprint</a>
        </div>
    </div>
</div>
<div id="fullpage">
    <div class="section main-top"  id="section0">
        <div class="slide"><img src="img/bg_head.jpg" alt=""></div>
        <div class="slide"><img src="img/bg_head.jpg" alt=""></div>
        <div class="slide"> <img src="img/bg_head.jpg" alt=""></div>
        <div class="slide"><img src="img/bg_head.jpg" alt=""> </div>
        <div class="main-head">
            <div class="left top-left-text">
                <div class="header-text">
                    <p>OFFICES</p>
                    <p>to Nurture</p>
                    <p>your businsess</p>
                </div>
                <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit</span>
                <button class="openPop">BOOK NOW</button>
            </div>
            <div class="right top-tight-img">
                <img src="img/main_head_plan.png" alt="">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="section" id="section1">
        <div class="main-head normal">
            @foreach($rooms as $room)
                <div class="slide">
                    <div class="image-slide">
                        <img src="img/bg_sec.jpg" alt="">
                    </div>
                    <div class="main-info">
                        <div class="left left-cost">
                            <div class="top">
                                <div class="left">
                                    <img src="img/2-layers.png" alt="">
                                </div>
                                <div class="right">
                                    <div class="cost-top">
                                        <h2>{{ $room->name }}</h2>
                                        <p>Zimmer 1</p>
                                        <div class="button">
                                            <button>Besetzt</button>
                                            <p> {{ $room->price }}€ <span> p.Mo</span></p>
                                        </div>
                                    </div>
                                    <div class="cost-bot">
                                        <ul>
                                            <li>
                                                <p>Surface: <span> {{ $room->square }} m2</span></p>
                                            </li>
                                            <li>
                                                <p>Arbeitsplätze: <span> {{ $room->floor }}</span></p>
                                            </li>
                                            <li>
                                                <p>Fenster: <span> {{ $room->windows }}</span></p>
                                            </li>
                                            <li>
                                                <p>Tische: <span> {{ $room->tables }}</span></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="button-main-cost">
                                <button class="openPop" onclick="openForm({{ $room->id }})">BOOK NOW</button>
                            </div>
                            <form style="display: none" class="form-{{ $room->id }}" method="post" action="{{ route('reserves.create') }}">
                                <input name="room_id" type="hidden" value="{{ $room->id }}">
                                First name
                                <input name="first_name" type="text"><br>
                                Last name
                                <input name="last_name" type="text"><br>
                                Email
                                <input name="email" type="email"><br>
                                Company
                                <input name="company" type="text"><br>
                                Phone
                                <input name="phone" type="text"><br>
                                To date
                                <input name="to_date" type="date"><br>
                                From date
                                <input name="from_date" type="date"><br>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" value="Submit">
                            </form>
                            <script>
                                function openForm(id) {
                                    $('.form-' + id).show();
                                }
                            </script>
                        </div>
                        <div class="right right-cost">
                            <img src="img/plan_sec.png" alt="">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="section" id="section2">
        <div class="main-head normal">
            <div class="amenities">
                <div class="amenities-items">
                    <div class="item">
                        <div class="img">
                            <img src="img/item1.png" alt="">
                        </div>
                        <p>Drucker</p>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                    </div>
                    <div class="item">
                        <div class="img">
                            <img src="img/item2.png" alt="">
                        </div>
                        <p>Internet</p>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                    </div>
                    <div class="item">
                        <div class="img">
                            <img src="img/item3.png" alt="">
                        </div>
                        <p>Conference Room</p>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                    </div>
                    <div class="item">
                        <div class="img">
                            <img src="img/item4.png" alt="">
                        </div>
                        <p>Kitchen</p>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                    </div>
                    <div class="item">
                        <div class="img">
                            <img src="img/item5.png" alt="">
                        </div>
                        <p>Keller</p>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                    </div>
                    <div class="item">
                        <div class="img">
                            <img src="img/item6.png" alt="">
                        </div>
                        <p>Gang-Schranke</p>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section3">
        <div class="main-head normal">
            <div class="left left-map">
                <div class="left-center">
                    <div class="top">
                        <p><span>U4 </span> Meidling <small> 7 mins of walking</small></p>
                        <p><span class="different">U6 </span> NiederhofstraSSE <small> 7 mins of walking</small></p>
                    </div>
                    <div class="bottom">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci autem eum ex exercitationem expedita incidunt, maiores molestias nisi nulla optio praesentium quam quibusdam recusandae rem, sint tempore velit vero. Adipisci.</p>

                    </div>
                </div>

            </div>
            <div class="right map-right">
                <div id="map"></div>
            </div>
        </div>
    </div>
    <div class="section section-last" id="section4">
        <div class="main-head normal last-section">
            <div class="slide">
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores aspernatur aut dicta ea eos esse est ex facere illo minima nulla odit placeat quos, recusandae sed veritatis voluptate voluptatem.</p>
                </div>
                <div class="name">
                    <p>RUDOLF<br>
                        Schwarzberg
                    </p>
                    <span>Position, Company Name</span>
                </div>
            </div>
            <div class="slide">
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores aspernatur aut dicta ea eos esse est ex facere illo minima nulla odit placeat quos, recusandae sed veritatis voluptate voluptatem.</p>
                </div>
                <div class="name">
                    <p>RUDOLF<br>
                        Schwarzberg
                    </p>
                    <span>Position, Company Name</span>
                </div>
            </div>
            <div class="slide">
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores aspernatur aut dicta ea eos esse est ex facere illo minima nulla odit placeat quos, recusandae sed veritatis voluptate voluptatem.</p>
                </div>
                <div class="name">
                    <p>RUDOLF<br>
                        Schwarzberg
                    </p>
                    <span>Position, Company Name</span>
                </div>
            </div>
            <div class="slide">
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores aspernatur aut dicta ea eos esse est ex facere illo minima nulla odit placeat quos, recusandae sed veritatis voluptate voluptatem.</p>
                </div>
                <div class="name">
                    <p>RUDOLF<br>
                        Schwarzberg
                    </p>
                    <span>Position, Company Name</span>
                </div>
            </div>
            <div class="slide">
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores aspernatur aut dicta ea eos esse est ex facere illo minima nulla odit placeat quos, recusandae sed veritatis voluptate voluptatem.</p>
                </div>
                <div class="name">
                    <p>RUDOLF<br>
                        Schwarzberg
                    </p>
                    <span>Position, Company Name</span>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
