<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customer\DeleteCustomerRequest;
use App\Http\Requests\Customer\GetCustomerRequest;
use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\User;

/**
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param UpdateCustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateCustomer(UpdateCustomerRequest $request)
    {
        $user = User::find($request->id);
        $user->update($request->all());

        return redirect(route('customer.list'));
    }

    /**
     * @param GetCustomerRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCustomer(GetCustomerRequest $request)
    {
        return view('admin.edit-customers', [
            'user' => User::find($request->id)
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listCustomers()
    {
        return view('admin.customers', [
            'users' => User::where('username', null)
                ->where('password', null)
                ->paginate(10)
        ]);
    }

    /**
     * @param DeleteCustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteCustomer(DeleteCustomerRequest $request)
    {
        $user = User::find($request->id);
        $user->delete();

        return redirect(route('customer.list'));
    }
}
