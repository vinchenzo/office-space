<?php

namespace App\Http\Controllers;
use App\Http\Requests\Reserves\ApproveReservesRequest;
use App\Http\Requests\Reserves\DeleteReservesRequest;
use App\Reserve;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;


/**
 * Class ReservesController
 * @package App\Http\Controllers
 */
class ReservesController extends Controller
{
    protected $client;
    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

        $client = new \Google_Client();
        $client->setAuthConfig(storage_path('app/laravel-google-calendar/client_id.json'));
        $client->addScope(\Google_Service_Calendar::CALENDAR);
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $this->client = $client;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listReserves()
    {
        return view('admin.reserves', [
            'reserves' => Reserve::paginate(10)
        ]);
    }

    /**
     * @param DeleteReservesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteReserves(DeleteReservesRequest $request)
    {
        $reserves = Reserve::find($request->id);
        $reserves->delete();

        return back();
    }

    /**
     * @param ApproveReservesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approveReserves(ApproveReservesRequest $request)
    {
        $reserved = Reserve::find($request->id);
        $reserved->update(['confirmed' => 1]);

        $this->client->setAccessToken(env('ACCESS_TOKEN_GOOGLE'));

        $service = new \Google_Service_Calendar($this->client);
        $calendarId = $reserved->room->calendar_id;

        $event = new \Google_Service_Calendar_Event([
            'summary' => 'User: ' . $reserved->user->first_name . ' Room: ' . $reserved->room->name,
            'description' => $reserved->room->name,
            'reminders' => ['useDefault' => true],
        ]);

        $setEnd = new \Google_Service_Calendar_EventDateTime();
        $setEnd->setDateTime(date_format(date_create($reserved->to_date), 'Y-m-d\TH:i:s.Z\Z'));
        $event->setEnd($setEnd);

        $setStart = new \Google_Service_Calendar_EventDateTime();
        $setStart->setDateTime(date_format(date_create($reserved->from_date), 'Y-m-d\TH:i:s.Z\Z'));
        $event->setStart($setStart);

        $results = $service->events->insert($calendarId, $event);

        if (!$results) {
            return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
        }

        Mail::send('email.approve', ['reserved_id' => $request->id],
            function ($m) use ($reserved) {
            $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $m->to($reserved->user->email, $reserved->user->first_name)
                ->subject('Booking');
        });

        Mail::send('email.approve', ['reserved_id' => $request->id],
            function ($m) use ($reserved) {
                $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $m->to(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                    ->subject('Booking');
        });

        return redirect(route('reserves.list'));
    }
}