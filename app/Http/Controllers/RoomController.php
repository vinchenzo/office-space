<?php

namespace App\Http\Controllers;

use App\Http\Requests\Room\DeleteRoomRequest;
use App\Http\Requests\Room\GetRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Room;

/**
 * Class RoomController
 * @package App\Http\Controllers
 */
class RoomController extends Controller
{
    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listRooms()
    {
        return view('admin.rooms', [
            'rooms' => Room::paginate(10)
        ]);
    }

    /**
     * @param UpdateRoomRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateRoom(UpdateRoomRequest $request)
    {
        $room = Room::find($request->id);
        $room->update($request->all());

        return redirect(route('room.list'));
    }

    /**
     * @param GetRoomRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRoom(GetRoomRequest $request)
    {
        return view('admin.edit-rooms', [
            'room' => Room::find($request->id)
        ]);
    }

    public function deleteRoom(DeleteRoomRequest $request)
    {
        $room = Room::find($request->id);
        $room->delete();

        return redirect(route('room.list'));
    }
}