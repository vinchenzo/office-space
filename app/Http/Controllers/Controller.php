<?php

namespace App\Http\Controllers;

use App\Http\Requests\Landing\CreateReservesRequest;
use App\Reserve;
use App\Room;
use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use ValidatesRequests;
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function landing()
    {
        return view('landing', [
            'rooms' => Room::all()
        ]);
    }

    /**
     * @param CreateReservesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createReserves(CreateReservesRequest $request)
    {
        $rooms = Reserve::where('room_id', $request->room_id)->get();

        foreach ($rooms as $room) {
            if (
                ($room->from_date < $request->from_date) &&
                ($room->to_date > $request->to_date) &&
                ($room->confirmed)
            ) {
                return redirect(route('landing'))->withErrors('Data busy');
            }
        }

        $user = User::create([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company' => $request->company,
            'phone' => $request->phone
        ]);

        Reserve::create([
            'user_id' => $user->id,
            'room_id' => $request->room_id,
            'from_date' => $request->from_date,
            'to_date' => $request->to_date,
            'confirmed' => 0
        ]);

        return redirect(route('landing'));
    }
}
