<?php

namespace App\Http\Requests\Reserves;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ApproveReservesRequest
 * @package App\Http\Requests\Reserves
 */
class ApproveReservesRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:reserves,id',
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $data = parent::all();
        $data['id'] = (int) $this->route('id');

        return $data;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}