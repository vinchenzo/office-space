<?php

namespace App\Http\Requests\Room;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DeleteRoomRequest
 * @package App\Http\Requests\Room
 */
class DeleteRoomRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:rooms,id',
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $data = parent::all();
        $data['id'] = (int) $this->route('id');

        return $data;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}