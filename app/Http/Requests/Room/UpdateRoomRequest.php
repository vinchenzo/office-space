<?php

namespace App\Http\Requests\Room;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRoomRequest
 * @package App\Http\Requests\Customer
 */
class UpdateRoomRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id'      => 'required|integer|exists:rooms,id',
            'name'    => 'string|min:3|max:30',
            'price'   => 'integer',
            'square'  => 'numeric',
            'floor'   => 'integer',
            'windows' => 'integer',
            'tables'  => 'integer'
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $data = parent::all();
        $data['id'] = (int) $this->route('id');

        return $data;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}