<?php

namespace App\Http\Requests\Landing;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateReservesRequest
 * @package App\Http\Requests\Landing
 */
class CreateReservesRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'room_id'    => 'required|integer|exists:rooms,id',
            'email'      => 'required|email|min:6|max:30',
            'first_name' => 'required|string|min:3|max:60',
            'last_name'  => 'required|string|min:3|max:60',
            'company'    => 'required|string|min:5|max:40',
            'phone'      => 'required|string|min:8|max:25',
            'to_date'    => 'required|date|after:from_date|regex:/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/',
            'from_date'  => 'required|date|after:yesterday|before:to_date|regex:/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/'
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}