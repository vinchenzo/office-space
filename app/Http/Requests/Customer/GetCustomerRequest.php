<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetCustomerRequest
 * @package App\Http\Requests\Customer
 */
class GetCustomerRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:users,id',
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $data = parent::all();
        $data['id'] = (int) $this->route('id');

        return $data;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}