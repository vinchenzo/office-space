<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateCustomerRequest
 * @package App\Http\Requests\Customer
 */
class UpdateCustomerRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id'         => 'required|integer|exists:users,id',
            'email'      => 'email|min:6|max:30',
            'first_name' => 'string|min:3|max:60',
            'last_name'  => 'string|min:3|max:60',
            'company'    => 'string|min:5|max:40',
            'phone'      => 'string|min:8|max:25'
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $data = parent::all();
        $data['id'] = (int) $this->route('id');

        return $data;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}