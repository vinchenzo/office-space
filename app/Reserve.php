<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Reserve
 * @package App
 */
class Reserve extends Model
{
    protected $fillable = [
        'user_id',
        'room_id',
        'confirmed',
        'from_date',
        'to_date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
