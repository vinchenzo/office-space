<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Room extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'square',
        'floor',
        'windows',
        'tables',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'reserves', 'room_id', 'user_id')->withPivot(['to_date', 'from_date']);
    }
}
