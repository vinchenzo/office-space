<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|-------------------
|Auth route
|-------------------
*/

Auth::routes();

/*
|-------------------
|Admin route
|-------------------
*/

Route::get('/admin', function () {
    return view('admin.admin');
})->middleware('auth');

/*
|-------------------
|Customer route
|-------------------
*/
Route::post('/customer/update/{id}', 'CustomerController@updateCustomer')->name('customer.update');

Route::get('/customers/list', 'CustomerController@listCustomers')->name('customer.list');

Route::get('/customer/get/{id}', 'CustomerController@getCustomer')->name('customer.get');

Route::get('/customer/delete/{id}', 'CustomerController@deleteCustomer')->name('customer.delete');

/*
|------------------
|Room route
|------------------
*/
Route::post('/room/update/{id}', 'RoomController@updateRoom')->name('room.update');

Route::get('/rooms/list', 'RoomController@listRooms')->name('room.list');

Route::get('/room/get/{id}', 'RoomController@getRoom')->name('room.get');

Route::get('/room/delete/{id}', 'RoomController@deleteRoom')->name('room.delete');

/*
|------------------
|Room route
|------------------
*/
Route::get('/invoices/list', 'ReservesController@listReserves')->name('reserves.list');

Route::get('/invoices/delete/{id}', 'ReservesController@deleteReserves')->name('reserves.delete');

Route::get('/invoices/approve/{id}', 'ReservesController@approveReserves')->name('reserves.approve');

/*
|------------------
|Landing route
|------------------
*/

Route::get('/', 'Controller@landing')->name('landing');

Route::post('/reserves', 'Controller@createReserves')->name('reserves.create');