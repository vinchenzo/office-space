<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            'name' => 'Avocado',
            'calendar_id' => 'o9j8556jv6rr2ioe03r44jib28@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => 1,
            'tables' => mt_rand(1, 10000),
        ]);

        DB::table('rooms')->insert([
            'name' => 'Mint',
            'calendar_id' => '4koo7cbnl9v3a73kqvu6vks748@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => 1,
            'tables' => mt_rand(1, 10000),
        ]);

        DB::table('rooms')->insert([
            'name' => 'Emirald',
            'calendar_id' => '5m5sqq8r2s1hk1qq89bpeg53to@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => mt_rand(1, 10000),
            'tables' => mt_rand(1, 10000),
        ]);

        DB::table('rooms')->insert([
            'name' => 'Kiwi',
            'calendar_id' => 'g84gojon11h7pgvqtnefrhg8e4@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => 2,
            'tables' => mt_rand(1, 10000),
        ]);

        DB::table('rooms')->insert([
            'name' => 'Lime',
            'calendar_id' => '2n3vrvjtgncgd8vjopscf318qg@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => 2,
            'tables' => mt_rand(1, 10000),
        ]);

        DB::table('rooms')->insert([
            'name' => 'Olive',
            'calendar_id' => '5bempg3lpkhjocafl8m0sshdbo@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => 1,
            'tables' => mt_rand(1, 10000),
        ]);

        DB::table('rooms')->insert([
            'name' => 'Wasabi',
            'calendar_id' => ' 07i2qllsakdu7vknf0vu589vps@group.calendar.google.com',
            'price' => mt_rand(1, 10000),
            'square' => mt_rand(1, 10000),
            'floor' => mt_rand(1, 10000),
            'windows' => 2,
            'tables' => mt_rand(1, 10000),
        ]);
    }
}
