<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'first_name' => 'Tanbruckgasse',
            'last_name' => 'Tanbruckgasse',
            'email' => 'office@tanbruck-offices.wien',
            'password' => bcrypt('Tanbruckgasse'),
            'company' => str_random(10),
            'phone' => mt_rand(8, 13)
        ]);
    }
}
