$(document).ready(function() {

	$('#fullpage').fullpage({
		menu: '#menu',
		lockAnchors: false,
		anchors: ['firstpage', 'secpage', 'thirdpage', 'fourthpage', 'fivethpage'],
		navigation: false,
		showActiveTooltip: true,
		slidesNavigation: true,
		autoScrolling: true,
		lazyLoading: false,
		fitToSection: false,
		slidesNavPosition: 'bottom',
		normalScrollElementTouchThreshold: 1,
		onLeave: function(index, nextIndex){
			if(nextIndex === 2 || nextIndex === 3 || nextIndex === 4 || nextIndex === 5){
					$('#menu').addClass('menu-black');
			}else{
				$('#menu').removeClass('menu-black');
			}
		}
	});
	if(window.matchMedia('(max-width: 1000px)').matches){
		$.fn.fullpage.setAutoScrolling(false);
		$('#menu ul li').click(function() {
			$('#menu').removeClass('active');
			$('#menu .buttons-menu .open-menu').removeClass('actives');
		});
		/*$('#fullpage').fullpage({
			autoScrolling: false
		});*/
	}
	$( ".buttons-menu" ).click(function() {
		$(this).parent().toggleClass('active');
		$(this).children('.open-menu').toggleClass('actives');
	});
	////// important ///
	// $.fn.fullpage.setAutoScrolling(false);



	// $.fn.fullpage.setMouseWheelScrolling(false);
	// $.fn.fullpage.setAllowScrolling(false);
	/*$('.links_move').on('click', 'a', function(e) {
		e.preventDefault();
		var id = $(this).attr('href'),
			top = $(id).offset().top - 100;
		$('body,html').animate({scrollTop: top}, 700);
	});*/
});

function initMap() {
	var uluru = {lat: 48.1780468, lng: 16.3287015};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
}